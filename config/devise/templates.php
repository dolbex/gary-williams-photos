<?php return array (
		'homepage' => array(
			'human_name' => 'Homepage',
			'extends' => null,
			'vars' => array(
				'photos' => 'App\Galleries\GalleriesRepository.getEveryPhoto',
			),
		),
		'admin.galleries.create' => array(
			'human_name' => 'Admin Create Galleries',
			'extends' => 'devise::admin.layouts.master',
		),
		'admin.galleries.edit' => array(
			'human_name' => 'Admin Edit Galleries',
			'extends' => 'devise::admin.layouts.master',
			'vars' => array(
				'gallery' => array(
					'App\Galleries\GalleriesRepository.getGallery' => array(
						'{params.galleryid}',
					),
				),
			),
		),
		'admin.galleries.index' => array(
			'human_name' => 'Admin Galleries Index',
			'extends' => 'devise::admin.layouts.master',
			'vars' => array(
				'galleries' => 'App\Galleries\GalleriesRepository.getAllGalleries',
			),
		),
	);