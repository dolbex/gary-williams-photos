<?php

class GalleryNewPagesSeeder extends DeviseSeeder
{
    public function run()
    {
        $pages = array (
  0 => 
  array (
    'language_id' => 45,
    'view' => 'admin.galleries.index',
    'title' => 'Admin Galleries',
    'http_verb' => 'get',
    'slug' => '/admin/galleries',
    'response_type' => 'View',
    'route_name' => 'admin-galleries-index',
    'before' => 'ifNotLoggedInGoToLogin',
  ),
  1 => 
  array (
    'language_id' => 45,
    'view' => 'admin.galleries.create',
    'title' => 'Admin Create Gallery',
    'http_verb' => 'get',
    'slug' => '/admin/galleries/create',
    'response_type' => 'View',
    'route_name' => 'admin-galleries-create',
    'before' => 'ifNotLoggedInGoToLogin',
  ),
  2 => 
  array (
    'language_id' => 45,
    'view' => 'admin.galleries.edit',
    'title' => 'Admin Edit Gallery',
    'http_verb' => 'get',
    'slug' => '/admin/galleries/edit/{galleryid}',
    'response_type' => 'View',
    'route_name' => 'admin-galleries-edit',
    'before' => 'ifNotLoggedInGoToLogin',
  ),
  3 => 
  array (
    'language_id' => 45,
    'view' => NULL,
    'title' => 'Admin Gallery Update',
    'http_verb' => 'put',
    'slug' => '/admin/galleries/update/{galleryid}',
    'response_type' => 'Function',
    'response_path' => 'App\\Galleries\\GalleriesResponseHandler.requestUpdate',
    'response_params' => 'params.galleryid, input',
    'route_name' => 'admin-galleries-update',
    'before' => 'ifNotLoggedInGoToLogin',
  ),
  4 => 
  array (
    'language_id' => 45,
    'view' => NULL,
    'title' => 'Admin Gallery Destroy',
    'http_verb' => 'delete',
    'slug' => '/admin/galleries/destroy/{galleryid}',
    'response_type' => 'Function',
    'response_path' => 'App\\Galleries\\GalleriesResponseHandler.requestDestroy',
    'response_params' => 'params.galleryid',
    'route_name' => 'admin-galleries-destroy',
    'before' => 'ifNotLoggedInGoToLogin',
  ),
  5 => 
  array (
    'language_id' => 45,
    'view' => NULL,
    'title' => 'Admin Gallery Store',
    'http_verb' => 'post',
    'slug' => '/admin/galleries/store',
    'response_type' => 'Function',
    'response_path' => 'App\\Galleries\\GalleriesResponseHandler.requestCreate',
    'response_params' => 'input',
    'route_name' => 'admin-galleries-store',
    'before' => 'ifNotLoggedInGoToLogin',
  ),
);

        $now = date('Y-m-d H:i:s', strtotime('now'));

        $dvsPages = $this->findOrCreateRows('dvs_pages', 'route_name', $pages);

        foreach ( $dvsPages as $dvsPage )
        {
            $this->findOrCreateRow('dvs_page_versions', 'page_id', [
                'page_id'            => $dvsPage->id,
                'created_by_user_id' => 1,
                'name'               => 'Default',
                'starts_at'          => $now,
            ]);
        }
    }

}
