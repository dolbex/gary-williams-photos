    <div class="dvs-form-group">
        <?= Form::label('Name') ?>
        <?= Form::text('name', null, array()) ?>
    </div>
    <div class="dvs-form-group">
        <?= Form::label('Description') ?>
        <?= Form::textarea('description', null, array()) ?>
    </div>
    <div class="dvs-form-group">
        <?= Form::label('Path') ?>
        <?= Form::text('path', null, array( 'class' => 'image-picker')) ?>
    </div>
