@extends('devise::admin.layouts.master')

@section('title')
    <div id="dvs-admin-title">
        <h1><span class=""></span>Galleries</h1>
    </div>
@stop

@section('subnavigation')
    <div id="dvs-admin-actions">
        <?= link_to(URL::route('admin-galleries-create'), 'Create New Gallery', array('class' => 'dvs-button dvs-button-secondary')) ?>
    </div>
@stop

@section('main')

   @if (!$galleries->count())

        <h3>No Gallery Entries Found.</h3>

    @else

        <table class="dvs-admin-table">
            <thead>
                <tr>
                        <th><?= Sort::link('name') ?></th>

                    <th><?= Sort::clearSortLink('Clear Sort', array('class'=>'dvs-button dvs-button-small dvs-button-outset')) ?></th>
                </tr>
            </thead>

            <tbody>
                @foreach($galleries as $gallery)
                    <tr>
                                <td><?= $gallery['name'] ?></td>

                        <td class="dvs-tac dvs-button-group">
                            <a class="dvs-button dvs-button-small" href="<?= route('admin-galleries-edit', $gallery->id) ?>">Edit</a>
                            <?= Form::delete(route('admin-galleries-destroy', $gallery->id), 'Delete', null, array('class' => 'dvs-button dvs-button-small dvs-button-danger')) ?>
                        </td>
                    </tr>
                @endforeach
            </tbody>

            <tfoot>
                <tr>
                    <td><?= $galleries->appends(Input::except(['page']))->render() ?></td>
                </tr>
            </tfoot>
        </table>

    @endif

@stop

@section('js')
    <script>devise.require(['app/admin/admin'])</script>
@stop