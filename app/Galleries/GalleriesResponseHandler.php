<?php namespace App\Galleries;

use Illuminate\Routing\Redirector;

class GalleriesResponseHandler
{
	function __construct(Redirector $Redirect, GalleriesManager $GalleriesManager)
	{
		$this->Redirect = $Redirect;
		$this->Manager  = $GalleriesManager;
	}

	function requestCreate($input)
	{
		if ($this->Manager->createGallery($input))
        {
            return $this->Redirect->route('admin-galleries-index')
                ->with('message', 'Gallery successfully created');
        }

        return $this->Redirect->route('admin-galleries-create')
                ->withInput()
                ->withErrors($this->Manager->errors)
                ->with('message', $this->Manager->message);
	}

	function requestUpdate($id, $input)
	{
		if ($this->Manager->updateGallery($id, $input))
        {
            return $this->Redirect->route('admin-galleries-index')
                ->with('message', 'Gallery successfully updated');
        }

        return $this->Redirect->route('admin-galleries-edit', $id)
                ->withInput()
                ->withErrors($this->Manager->errors)
                ->with('message', $this->Manager->message);
	}

	function requestDestroy($id)
	{
		$this->Manager->destroyGallery($id);

        return $this->Redirect->route('admin-galleries-index')
                ->with('message', 'Gallery successfully removed');
	}

}