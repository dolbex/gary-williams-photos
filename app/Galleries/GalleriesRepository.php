<?php namespace App\Galleries;

use  App\Gallery;

class GalleriesRepository
{
	function __construct(Gallery $gallery)
	{
		$this->Gallery = $gallery;
	}

	function getGallery($id)
	{
		return $this->Gallery->findorfail($id);
	}

	function getAllGalleries()
	{
		return $this->Gallery->paginate();
	}

	function getEveryPhoto()
	{
		return $this->Gallery->orderBy('id', 'DESC')->get();
	}
}