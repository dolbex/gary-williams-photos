<?php namespace App\Galleries;

use App\Gallery;
use Devise\Support\Framework;
use Devise\Media\Images\Manager as ImageManager;
use Devise\Media\Categories\CategoryPaths;

class GalleriesManager
{

    /**
     * Framework components being used from Laravel's framework
     *
     * @var Framework
     */
    protected $Framework;

    /**
     * Errors are kept in an array and can be
     * used later if validation fails and we want to
     * know why
     *
     * @var array
     */
    public $errors;

    /**
     * Validation messages
     */
    public $messages = array(
        // 'title.required' => 'Title required.',
    );

	function __construct(Gallery $gallery, Framework $Framework, CategoryPaths $CategoryPaths, ImageManager $ImageManager)
	{
		$this->Gallery = $gallery;
        $this->Validator = $Framework->Validator;
        $this->CategoryPaths = $CategoryPaths;
		$this->ImageManager = $ImageManager;
	}

	/**
     * Create validation rules
     *
     * @return array
     */
	public function createRules()
	{
 		return array(
        	// 'title' => 'required',
    	);
	}

	/**
	 * Create a new gallery
	 *
	 * @param  array $input
	 * @return Gallery $gallery
	 */
	public function createGallery($input)
	{
        $validator = $this->Validator->make($input, $this->createRules(), array("Could not create new gallery"));

        if ($validator->passes())
        {
    		$gallery = $this->Gallery;

            $gallery->name = $input['name'];
            $gallery->description = $input['description'];
            $gallery->path = $input['path'];

            $image = \Image::make(public_path() . $input['path']);

            $gallery->width = $image->width();
            $gallery->height = $image->height();
            $gallery->thumb_path = $this->cropImage($input['path'], $image);

    		$gallery->save();

    		return $gallery;
        }

        $this->errors = $validator->errors()->all();
        $this->message = "There were validation errors.";
        return false;
	}

	/**
     * Update validation rules
     *
     * @return array
     */
	public function updateRules()
	{
 		return array(
        	// 'title' => 'min:3'
    	);
	}

	/**
	 * Update a gallery
	 *
	 * @param  integer $id
	 * @param  array $input
	 * @return Gallery $gallery
	 */
	public function updateGallery($id, $input)
	{
        $validator = $this->Validator->make($input, $this->updateRules($id, $input), array("Could not update gallery"));

		if ($validator->passes())
        {
    		$gallery = $this->Gallery->findOrFail($id);

            $gallery->name = $input['name'];
            $gallery->description = $input['description'];
            $gallery->path = $input['path'];

            $image = \Image::make(public_path() . $input['path']);

            $gallery->width = $image->width();
            $gallery->height = $image->height();
            $gallery->thumb_path = $this->cropImage($input['path'], $image);

    		$gallery->save();

    		return $gallery;
        }

        $this->errors = $validator->errors()->all();
        $this->message = "There were validation errors.";
        return false;
	}

	/**
	 * Delete a Spotlight
	 *
	 * @param  integer $id
	 * @return Spotlight $Spotlight
	 */
	public function destroyGallery($id)
	{
		$gallery = $this->Gallery->findOrFail($id);
		$gallery->delete();

		return $gallery;
	}


    private function cropImage($path, $img) {
        $img->fit(300, 200);
        $thumb_path = public_path() . '/thumbnails/' . basename($path);
        $img->save($thumb_path, 70);

        return '/thumbnails/' . basename($path);
    }

}