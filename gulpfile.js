var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss', 'resources/assets/css/app.css');
});

elixir(function(mix) {
    mix.styles([
    	'vendor/normalize.min.css',
    	'vendor/photoswipe.css',
    	'vendor/photoswipe-default-skin.css',
        'app.css'
    ], 'public/css/all.css');
});

elixir(function(mix) {
    mix.scripts([
    	'vendor/jquery-2.1.4.min.js',
    	'vendor/modernizr-2.8.3.min.js',
        'vendor/photoswipe-ui-default.js',
        'vendor/photoswipe.js',
        'app.js',
    ], 'public/js/app.js');
});

